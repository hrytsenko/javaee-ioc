# About

Example implementation of IoC and DI design principles.

Keywords: _bytecode_, _class loader_, _reflection_, _proxy_.

## Container

The [IoC] container is responsible for creating and configuring application components.
So, we use the container to create an instance of the `Foo` component.

```java
Foo foo = container.getInstance(Foo.class);
String result = foo.execute();
```

We suppose that the container transparently creates and injects required dependencies.
So, we expect that the container initializes the `bar` field, when it creates an instance of the `Foo` component.

```java
@Component
public class Foo {

    private Bar bar;

    public String execute() {
        return "foo" + bar.execute();
    }
}
```

The component is the annotated Java class.
Annotations provide information about component for the container.
For example, we use the `@Component` annotation to identify components.

## Proxy

The container uses dynamic proxies to manage access to components.
So, the container returns the proxy instance instead of the component instance.
The proxy class inherits the component class, so, the client may use the proxy instance as the component instance.

The dynamic class can be created in runtime using the bytecode manipulation.
We use the [Javassist] library for generating bytecode in runtime.
We create a proxy class in runtime and then we define that this class inherits the component class.

```java
String name = component.getName();
CtClass target = pool.getCtClass(name);
CtClass proxy = pool.makeClass(name + "$Proxy");
proxy.setSuperclass(target);
```

We add the `$container` field to the dynamic proxy.
This field contains reference to the parent container.
So, the proxy instance is able to access the container instance.

```java
proxy.addField(CtField.make("private hrytsenko.impl.Container $container;", proxy));
```

Then, we add the constructor that initializes the `$container` field.
This constructor will be used by the container to create an instance of the proxy class.

```java
CtConstructor constructor = new CtConstructor(new CtClass[] { container }, proxy);
constructor.setBody("{ $container = $1; }");
proxy.addConstructor(constructor);
```

Also, we create a wrapper for each method of the component.
This wrapper notifies the container before and after the invocation of the component method.
So, the container can transparently manage the interaction of components.

```java
String name = method.getName();
CtMethod wrapper = CtNewMethod.copy(method, proxy, null);
String body = String.format("{ $container.before(this, \"%s\"); return $proceed($$); }", name);
wrapper.setBody(body, "super", name);
wrapper.insertAfter(String.format("{ $container.after(this, \"%s\"); }", name), true);
proxy.addMethod(wrapper);
```

As result, the proxy class for the `Foo` class will look like following:

```java
public class Foo$Proxy extends Foo {

    private Container $container;

    public Foo$Proxy(Container $1) {
        $container = $1;
    }

    public void execute() {
        $container.before(this, "execute");
        try {
            super.execute();
        } finally {
            $container.after(this, "execute");
        }
    }
}
```

When the proxy class is ready to use, it can be loaded using the class loader.
Then, we can create a proxy object using the reflection using the generated constructor.

```java
Constructor<?> constructor = proxy.getConstructor(Container.class);
Object instance = constructor.newInstance(this);
```

Finally, the container injects required dependencies into the proxy object.
We find all fields that refer to components.
Then, we obtain required components from the container.

```java
Object dependency = getInstance(field.getType());
field.set(instance, dependency);
```

# Tasks

1. Add caching of proxy classes.
1. Provide support for `javax.inject.Inject`.
1. Provide support for `javax.inject.Singleton`.
1. Provide support for `javax.validation.constraints.NotNull` for arguments.

  [IoC]: <http://www.martinfowler.com/articles/injection.html>
  [Javassist]: <http://jboss-javassist.github.io/javassist/>
