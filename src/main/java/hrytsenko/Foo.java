package hrytsenko;

import hrytsenko.api.Component;

@Component
public class Foo {

    private Bar bar;

    public String execute() {
        return "foo" + bar.execute();
    }

}
