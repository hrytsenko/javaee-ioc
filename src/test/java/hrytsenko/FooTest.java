package hrytsenko;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import hrytsenko.impl.Container;

public class FooTest {

    @Test
    public void execute() {
        Container container = new Container();

        Foo foo = container.getInstance(Foo.class);
        String result = foo.execute();

        assertThat(result, is(equalTo("foobar")));
    }

}
