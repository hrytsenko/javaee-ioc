package hrytsenko.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

import hrytsenko.api.Component;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtConstructor;
import javassist.CtField;
import javassist.CtMethod;
import javassist.CtNewMethod;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Container {

    @SneakyThrows
    @SuppressWarnings("unchecked")
    public <T> T getInstance(Class<T> component) {
        log.debug("Create a proxy for {}.", component);
        Class<?> proxy = createProxy(component);

        log.debug("Create an instance of {}.", proxy);
        Constructor<?> constructor = proxy.getConstructor(Container.class);
        Object instance = constructor.newInstance(this);

        injectDependencies(component, instance);

        log.debug("Return the instance {}.", instance);
        return (T) instance;
    }

    @SneakyThrows
    private <T> Class<?> createProxy(Class<T> component) {
        ClassPool pool = ClassPool.getDefault();

        String name = component.getName();
        CtClass target = pool.getCtClass(name);

        CtClass proxy = pool.makeClass(name + "$Proxy");
        proxy.setSuperclass(target);

        CtClass container = pool.get(Container.class.getName());
        configureProxy(container, target, proxy);

        return proxy.toClass();
    }

    @SneakyThrows
    private void configureProxy(CtClass container, CtClass component, CtClass proxy) {
        proxy.addField(CtField.make("private hrytsenko.impl.Container $container;", proxy));

        CtConstructor constructor = new CtConstructor(new CtClass[] { container }, proxy);
        constructor.setBody("{ $container = $1; }");
        proxy.addConstructor(constructor);

        for (CtMethod method : component.getDeclaredMethods()) {
            String name = method.getName();
            CtMethod wrapper = CtNewMethod.copy(method, proxy, null);
            String body = String.format("{ $container.before(this, \"%s\"); return $proceed($$); }", name);
            wrapper.setBody(body, "super", name);
            wrapper.insertAfter(String.format("{ $container.after(this, \"%s\"); }", name), true);
            proxy.addMethod(wrapper);
        }
    }

    @SneakyThrows
    private void injectDependencies(Class<?> component, Object instance) {
        for (Field field : component.getDeclaredFields()) {
            if (field.getType().isAnnotationPresent(Component.class)) {
                log.debug("Inject {}__{}.", instance, field.getName());
                field.setAccessible(true);
                Object dependency = getInstance(field.getType());
                field.set(instance, dependency);
            }
        }
    }

    public void before(Object obj, String method) {
        log.debug("Before {}__{}.", obj, method);
    }

    public void after(Object obj, String method) {
        log.debug("After {}__{}.", obj, method);
    }

}
